<section class="cta">
      <h3 class="cta__title">Getting a Headache Working on your Website?</h3>
      <p class="cta__description">Read 5 steps designed to ease your pain and create the website of your dreams.</p>
        <section class="cta__form" id="mc_embed_signup">
          <form class="validate" id="mc-embedded-subscribe-form" action="http://joshmedeski.us4.list-manage1.com/subscribe/post?u=8aa8c0b10e1c854c79edcbca6&amp;id=6ac5d6be9c" method="post" name="mc-embedded-subscribe-form" novalidate="" target="_blank">
            <div class="mc-field-group">
              <label class="cta__form__icon icon ion-person" for="mce-TEXT7"></label>
              <input class="radius cta__field" id="mce-TEXT7" type="text" name="mce-TEXT7" placeholder="Enter Your First Name" value="" />
              <label class="cta__form__icon icon ion-email" for="mce-EMAIL"></label>
              <input class="required email radius cta__field" id="mce-EMAIL" type="email" name="mce-EMAIL" placeholder="Enter Your Email Address" value="" />
              <input class="cta__button expand large button" id="mc-embedded-subscribe" type="submit" name="subscribe" value="Let's Do This!" />
              <div style="position: absolute; left: -5000px;"><input type="text" name="b_8aa8c0b10e1c854c79edcbca6_6ac5d6be9c" value="" /></div>
            </div>
          </form>
        </section>
  </section>
