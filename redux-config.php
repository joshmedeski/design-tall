<?php
if (!function_exists('redux_init')) :
  function redux_init() {
    $args = array();

    ob_start();

    $args['opt_name']       = 'rootbeer_options';
    $args['customizer']     = true;
    $args['display_name']   = 'Options';
    $args['google_api_key'] = 'AIzaSyAX_2L_UzCDPEnAHTG7zhESRVpMPS4ssII';
    $args['page_slug']      = 'rootbeer_options';
    $args['intro_text']     = ('<p>Thank you for using Rootbeer, I hope it helps you as much as it has helped me :)</p>');

    $args['share_icons']['twitter'] = array(
      'link'  => 'http://twitter.com/joshmedeski',
      'title' => 'Follow me on Twitter',
      'img'   => ReduxFramework::$_url . 'assets/img/social/Twitter.png'
      );
    $args['footer_text'] = __('<p>If all else fails, please shoot me a Tweet and I can help you out!</p>', 'rootbeer-framework');

    $sections = array();

    $sections[] = array(
      'title'  => __('General', 'rootbeer-framework'),
      'icon'   => 'el-icon-globe',
      'desc'   => __('Description goes here with html', 'rootbeer-framework'),
      'fields' => array(
        array(
          'id'       =>'footer-text',
          'type'     => 'editor',
          'title'    => __('Footer Text', 'redux-framework-demo'),
          'subtitle' => __('You can use the following shortcodes in your footer text: [wp-url] [site-url] [theme-url] [login-url] [logout-url] [site-title] [site-tagline] [current-year]', 'redux-framework-demo'),
          'default'  => 'Powered by [wp-url]. Built on the [theme-url].',
          ),
        array(
          'id'       =>'js-code',
          'type'     => 'ace_editor',
          'title'    => __('Tracking Code', 'redux-framework-demo'),
          'subtitle' => __('Paste your Google Analytics (or other) tracking code here. This will be added into the footer template of your theme.', 'redux-framework-demo'),
          'mode'     => 'javascript',
          'theme'    => 'carnary',
          ),
        ));

$sections[] = array(
  'title'  => __('Logo', 'rootbeer-framework'),
  'icon'   => 'el-icon-torso',
  'desc'   => __('Description goes here with html', 'rootbeer-framework'),
  'fields' => array(
    ));

$sections[] = array(
  'title'  => __('Styles', 'rootbeer-framework'),
  'icon'   => 'el-icon-brush',
  'desc'   => __('Description goes here with html', 'rootbeer-framework'),
  'fields' => array(
    array(
      'id'       =>'color-brand',
      'type'     => 'color',
      'output'   => array('a'),
      'title'    => __('Brand Color', 'rootbeer-framework'),
      'subtitle' => __('Pick a background color for the theme (default: blue).', 'rootbeer-framework'),
      'default'  => '#008cba',
      'validate' => 'color',
      ),
    array(
      'id'       =>'body-font',
      'type'     => 'typography',
      'title'    => __('Body Font', 'rootbeer-framework'),
      'subtitle' => __('Specify the body font properties.', 'rootbeer-framework'),
      'google'   =>true,
      'default'  => array(
        'color'       =>'#000000',
        'font-size'   =>'30px',
        'font-family' =>'Arial, Helvetica, sans-serif',
        'font-weight' =>'Normal',
        ),
      ),
    array(
      'id'       =>'heading-font',
      'type'     => 'typography',
      'title'    => __('Heading Font', 'rootbeer-framework'),
      'subtitle' => __('Specify the title font properties.', 'rootbeer-framework'),
      'google'   =>true,
      'default'  => array(
        'color'       =>'#333333',
        'font-size'   =>'30px',
        'font-family' =>'Arial, Helvetica, sans-serif',
        'font-weight' =>'Normal',
        ),
      ),
    ),
);

$sections[] = array(
  'title'  => __('Layout', 'rootbeer-framework'),
  'icon'   => 'el-icon-website',
  'desc'   => __('Description goes here with html', 'rootbeer-framework'),
  'fields' => array(
    array(
      'id'=>'image_select',
      'type' => 'image_select',
      'title' => __('Header', 'redux-framework-demo'),
      'subtitle' => __('This is the default layout for all pages.', 'redux-framework-demo'),
      'options' => array(
        '1' => array('alt' => '2 Column Right', 'img'  => ReduxFramework::$_url.'assets/img/2cr.png'),
        '2' => array('alt' => '2 Column Left', 'img'   => ReduxFramework::$_url.'assets/img/2cl.png'),
        '3' => array('alt' => '1 Column', 'img'        => ReduxFramework::$_url.'assets/img/1col.png')
            ),//Must provide key => value(array:title|img) pairs for radio options
      'default' => '1'
      ),
    array(
      'id'       =>'image_select',
      'type'     => 'image_select',
      'title'    => __('Navigation', 'redux-framework-demo'),
      'subtitle' => __('This is the default layout for all pages.', 'redux-framework-demo'),
      'options'  => array(
        '1' => array('alt' => '2 Column Right', 'img'  => ReduxFramework::$_url.'assets/img/2cr.png'),
        '2' => array('alt' => '2 Column Left', 'img'   => ReduxFramework::$_url.'assets/img/2cl.png'),
        '3' => array('alt' => '1 Column', 'img'        => ReduxFramework::$_url.'assets/img/1col.png')
            ),//Must provide key => value(array:title|img) pairs for radio options
      'default' => '1'
      ),
    array(
      'id'       =>'image_select',
      'type'     => 'image_select',
      'title'    => __('Page', 'redux-framework-demo'),
      'subtitle' => __('This is the default layout for all pages.', 'redux-framework-demo'),
      'options'  => array(
        '1' => array('alt' => '2 Column Right', 'img'  => ReduxFramework::$_url.'assets/img/2cr.png'),
        '2' => array('alt' => '2 Column Left', 'img'   => ReduxFramework::$_url.'assets/img/2cl.png'),
        '3' => array('alt' => '1 Column', 'img'        => ReduxFramework::$_url.'assets/img/1col.png'),
        '4' => array('alt' => '3 Column Middle', 'img' => ReduxFramework::$_url.'assets/img/3cm.png'),
        '5' => array('alt' => '3 Column Left', 'img'   => ReduxFramework::$_url.'assets/img/3cl.png'),
        '6' => array('alt' => '3 Column Right', 'img'  => ReduxFramework::$_url.'assets/img/3cr.png')
            ),//Must provide key => value(array:title|img) pairs for radio options
      'default' => '1'
      ),
    ));

$sections[] = array(
  'title'  => __('Widgets', 'rootbeer-framework'),
  'icon'   => 'el-icon-puzzle',
  'desc'   => __('Description goes here with html', 'rootbeer-framework'),
  'fields' => array(
    ));

$sections[] = array(
  'type' => 'divide',
  );

$sections[] = array(
  'title'  => __('Opt In', 'rootbeer-framework'),
  'icon'   => 'el-icon-envelope',
  'desc'   => __('Description goes here with html', 'rootbeer-framework'),
  'fields' => array(
    array(
      'id'       =>'Signup Code',
      'type'     => 'editor',
      'title'    => __('Footer Text', 'redux-framework-demo'),
      'subtitle' => __('You can use the following shortcodes in your footer text: [wp-url] [site-url] [theme-url] [login-url] [logout-url] [site-title] [site-tagline] [current-year]', 'redux-framework-demo'),
      'default'  => 'Powered by [wp-url]. Built on the [theme-url].',
      ),
    ));

$sections[] = array(
  'title'  => __('Social Media', 'rootbeer-framework'),
  'icon'   => 'el-icon-twitter',
  'desc'   => __('Description goes here with html', 'rootbeer-framework'),
  'fields' => array(
    array(
      "id"       => "socialmedia-icons",
      "type"     => "sorter",
      "title"    => "Social Media Links",
      "subtitle" => "Organize what links you want enabled and in what order.",
      "compiler" =>'true',
      "options"  => array(
        "Enabled" => array(
          "twitter"  => "Twitter",
          "facebook" => "Facebook",
          "rss"      => "RSS",
          ),
        "Disabled" => array(
          "dribbble"   => "Dribbble",
          "flickr"     => "Flickr",
          "github"     => "Github",
          "googleplus" => "Google Plus",
          "pinterest"  => "Pinterest",
          "youtube"    => "YouTube",
          "linkedin"   => "Linked In",
          ),
        ),
      ),
    array(
      'id'       =>'twitter',
      'type'     => 'text',
      'title'    => __('Twitter', 'redux-framework-demo'),
      'subtitle' => __('Please enter the full url', 'redux-framework-demo'),
      'validate' => 'url',
      'default'  => 'http://twitter.com/username'
      ),
    array(
      'id'       =>'facebook',
      'type'     => 'text',
      'title'    => __('Facebook', 'redux-framework-demo'),
      'subtitle' => __('Please enter the full url', 'redux-framework-demo'),
      'validate' => 'url',
      'default'  => 'http://facebook.com/pagename'
      ),
    array(
      'id'       =>'googleplus',
      'type'     => 'text',
      'title'    => __('Google Plus', 'redux-framework-demo'),
      'subtitle' => __('Please enter the full url', 'redux-framework-demo'),
      'validate' => 'url',
      'default'  => 'http://plus.google.com/username'
      ),
    array(
      'id'       =>'linkedin',
      'type'     => 'text',
      'title'    => __('Linked In', 'redux-framework-demo'),
      'subtitle' => __('Please enter the full url', 'redux-framework-demo'),
      'validate' => 'url',
      'default'  => 'http://linkedin.com/username'
      ),
    array(
      'id'       =>'pinterest',
      'type'     => 'text',
      'title'    => __('Pinterest', 'redux-framework-demo'),
      'subtitle' => __('Please enter the full url', 'redux-framework-demo'),
      'validate' => 'url',
      'default'  => 'http://pinterest.com/username'
      ),
    array(
      'id'       =>'dribbble',
      'type'     => 'text',
      'title'    => __('Dribbble', 'redux-framework-demo'),
      'subtitle' => __('Please enter the full url', 'redux-framework-demo'),
      'validate' => 'url',
      'default'  => 'http://dribbble.com/username'
      ),
    array(
      'id'       =>'github',
      'type'     => 'text',
      'title'    => __('Github', 'redux-framework-demo'),
      'subtitle' => __('Please enter the full url', 'redux-framework-demo'),
      'validate' => 'url',
      'default'  => 'http://github.com/username'
      ),
    array(
      'id'       =>'Flickr',
      'type'     => 'text',
      'title'    => __('Flicker', 'redux-framework-demo'),
      'subtitle' => __('Please enter the full url', 'redux-framework-demo'),
      'validate' => 'url',
      'default'  => 'http://flickr.com/username'
      ),
    array(
      'id'       =>'rss',
      'type'     => 'text',
      'title'    => __('RSS', 'redux-framework-demo'),
      'subtitle' => __('Please enter the full url', 'redux-framework-demo'),
      'validate' => 'url',
      'default'  => 'http://yourwebsite.com/feed'
      ),
    array(
      'id'       =>'youtube',
      'type'     => 'text',
      'title'    => __('YouTube', 'redux-framework-demo'),
      'subtitle' => __('Please enter the full url', 'redux-framework-demo'),
      'validate' => 'url',
      'default'  => 'http://youtube.com/username'
      ),
    ));

$sections[] = array(
  'title'  => __('Landing Pages', 'rootbeer-framework'),
  'icon'   => 'el-icon-graph',
  'desc'   => __('Description goes here with html', 'rootbeer-framework'),
  'fields' => array(
    array(
      'id'       =>'landing-activate',
      'type'     => 'switch',
      'title'    => __('Landing Page Template', 'redux-framework-demo'),
      'subtitle' => __('Disable this if you do not want use landing pages.'),
      "default"  => 1,
      'on'       => 'Enabled',
      'off'      => 'Disabled',
      ),
    ));

$sections[] = array(
  'title'  => __('Coming Soon', 'rootbeer-framework'),
  'icon'   => 'el-icon-time',
  'desc'   => __('Description goes here with html', 'rootbeer-framework'),
  'fields' => array(
    array(
      'id'      =>'comingsoon-activate',
      'type'    => 'switch',
      'title'   => __('Coming Soon Page', 'redux-framework-demo'),
      "default" => 0,
      'on'      => 'Enabled',
      'off'     => 'Disabled',
      ),
    array(
      'id'       =>'17',
      'type'     => 'date',
      'title'    => __('Launch Date', 'redux-framework-demo'),
      'subtitle' => __('When is your site going live?', 'redux-framework-demo'),
      'desc'     => __('This will only show the coming soon date, you will have to disable the coming soon page to activate your site.', 'redux-framework-demo')
      ),
    array(
      'id'    =>'comingsoon-headline',
      'type'  => 'text',
      'title' => __('Headline', 'redux-framework-demo'),
      'default' => __('Coming Soon', 'redux-framework-demo'),
      ),
    array(
      'id'       =>'comingsoon-message',
      'type'     => 'editor',
      'title'    => __('Message', 'redux-framework-demo'),
      'subtitle' => __('Give your audience a message to inform them about what is coming.'),
      'default'  => 'The site is coming soon, thanks for being patient.',
      ),
    ));

global $ReduxFramework;
$ReduxFramework = new ReduxFramework($sections, $args);
}
add_action('init', 'redux_init');
endif;
?>
