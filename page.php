<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
  <div class="row">
    <div class="medium-8 columns">
      <div class="box">
      <h2><?php the_title(); ?></h2>
      <hr>
        <?php the_content(); ?>
      </div>
    <?php endwhile // end of the loop. ?>
  </div>
  <div class="medium-4 columns">
  <?php get_sidebar(); ?>
  </div>
</div>

<?php get_footer(); ?>
