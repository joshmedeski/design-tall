      <!-- copyright and secondary navigation -->
      <footer class="footer">
        <div class="row">
          <div class="small-12 columns">
          <div class="logo--footer">
              <i class="logo__icon icon ion-man"></i>
              <i class="logo__icon icon ion-man"></i>
              <a href="<?php echo home_url(); ?>"><i class="logo__icon--tall icon ion-man"></i></a>
              <i class="logo__icon icon ion-man"></i>
            </div>
            <p class="footer__copyright">
              &copy; 2014 Design Tall.
            </p>
          </div>
        </div>
      </footer>

      <?php wp_footer(); ?>
      <script src="<?php bloginfo( 'template_url' ); ?>/js/jquery.min.js"></script>
      <script src="<?php bloginfo( 'template_url' ); ?>/js/foundation.min.js"></script>
      <script src="<?php bloginfo( 'template_url' ); ?>/js/app.js"></script>
    </body>
    </html>
