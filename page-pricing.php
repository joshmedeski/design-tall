<?php get_header(); ?>

<div class="row">
  <div class="small-12 columns">
  <h1>Getting Down to Business</h1>
  </div>
</div>

<div class="row collapse pricing">
  <div class="medium-3 columns">
    <ul class="pricing-table details">
      <li class="title">Package</li>
      <li class="price">Services</li>
      <li class="bullet-item">Website Design</li>
      <li class="bullet-item">Opt-In Offer</li>
      <li class="bullet-item">Analytics</li>
      <li class="bullet-item">Logo Design</li>
      <li class="bullet-item">Custom Graphics</li>
      <li class="bullet-item">Social Media</li>
      <li class="bullet-item">Landing Page</li>
      <li class="bullet-item">Online Store</li>
      <li class="bullet-item">eBook Design</li>
      <li class="bullet-item">Business Cards</li>
    </ul>
  </div>
  <div class="medium-3 columns">
    <ul class="pricing-table option">
      <li class="title">Basic</li>
      <li class="price">$1,200</li>
      <li class="bullet-item yes"></li>
      <li class="bullet-item yes"></li>
      <li class="bullet-item yes"></li>
      <li class="bullet-item no"></li>
      <li class="bullet-item no"></li>
      <li class="bullet-item no"></li>
      <li class="bullet-item no"></li>
      <li class="bullet-item no"></li>
      <li class="bullet-item no"></li>
      <li class="bullet-item no"></li>
    </ul>
  </div>
  <div class="medium-3 columns">
    <ul class="pricing-table option">
      <li class="title">Standard</li>
      <li class="price">$3,000</li>
      <li class="bullet-item yes"></li>
      <li class="bullet-item yes"></li>
      <li class="bullet-item yes"></li>
      <li class="bullet-item yes"></li>
      <li class="bullet-item yes"></li>
      <li class="bullet-item yes"></li>
      <li class="bullet-item no"></li>
      <li class="bullet-item no"></li>
      <li class="bullet-item no"></li>
      <li class="bullet-item no"></li>
    </ul>
  </div>
  <div class="medium-3 columns">
    <ul class="pricing-table option">
      <li class="title">Elite</li>
      <li class="price">$5,600</li>
      <li class="bullet-item yes"></li>
      <li class="bullet-item yes"></li>
      <li class="bullet-item yes"></li>
      <li class="bullet-item yes"></li>
      <li class="bullet-item yes"></li>
      <li class="bullet-item yes"></li>
      <li class="bullet-item yes"></li>
      <li class="bullet-item yes"></li>
      <li class="bullet-item yes"></li>
      <li class="bullet-item yes"></li>
    </ul>
  </div>
</div>

<a class="secondary button" href="#" data-reveal-id="details" data-reveal>Details</a>

<div id="details" class="reveal-modal" data-reveal>
  <h2>Detailed Description</h2>
  <p class="lead">Interested in working with me? Great! Here are some details about each item to clarify any confusion or vagueness (yes, I just made up a word).</p>
  <h3>Website Design</h3>
  <p></p>
  <h3>Opt-In Offer</h3>
  <p></p>
  <h3>Analytics</h3>
  <p></p>
  <h3>Logo Design</h3>
  <p></p>
  <h3>Custom Graphics</h3>
  <p></p>
  <h3>Social Media</h3>
  <p></p>
  <h3>Landing Page</h3>
  <p></p>
  <h3>Online Store</h3>
  <p></p>
  <h3>eBook Design</h3>
  <p></p>
  <a class="close-reveal-modal">&#215;</a>
</div>

<?php get_footer(); ?>
