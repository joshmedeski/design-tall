<?php
/* Functions
 *
 * @package    WordPress
 * @subpackage RootBeer
 * @author     JoshMedeski
 */

include_once('functions/styles.php');
// include_once('redux-config.php');

// This theme uses wp_nav_menu() in two locations.
register_nav_menus( array(
  'primary'   => __( 'Primary menu on the top', 'rootbeer' ),
  'secondary' => __( 'Secondary menu on the bottom', 'rootbeer' ),
  ) );

// Site Style
function rootbeer_site_style() {
  wp_enqueue_style( 'style', get_stylesheet_uri() );
}
add_action( 'wp_enqueue_scripts', 'rootbeer_site_style' );

// Sidebar Widgets
function rootbeer_sidebar_widgets() {
  register_sidebar( array(
    'name'          => __( 'Sidebar Widgets', 'rootbeer' ),
    'description'   => __( 'These are the widgets in the sidebar.', 'rootbeer' ),
    'id'            => 'sidebar-widgets',
    // 'class'         => 'medium-4 columns',
    'before_widget' => '<div class="panel">',
    'after_widget'  => '</div>',
    'before_title'  => '<h4 class="widgets--sidebar__title">',
    'after_title'   => '</h4>'
  ) );
}
add_action( 'widgets_init', 'rootbeer_sidebar_widgets' );

?>
