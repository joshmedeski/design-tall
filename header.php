<!doctype html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title><?php bloginfo('name'); ?></title>
  <link rel="shortcut icon" href="<?php bloginfo( 'template_url' ); ?>/favicon.ico">
  <script src="<?php bloginfo( 'template_url' ); ?>/js/modernizr.js"></script>
  <?php wp_head(); ?>
</head>
<body>
  <header class="header">
    <div class="row">
      <div class="medium-2 columns">
        <div class="logo">
          <i class="logo__icon icon ion-man"></i>
          <i class="logo__icon icon ion-man"></i>
          <a href="<?php echo home_url(); ?>"><i class="logo__icon--tall icon ion-man"></i></a>
          <i class="logo__icon icon ion-man"></i>
          <h1><?php bloginfo('name'); ?></h1>
        </div>
      </div>
      <div class="medium-10 columns">
        <?php
        $defaults = array(
          'theme_location'  => 'primary',
          'container'       => 'nav',
          'container_class' => 'navigation',
          'menu_class'      => 'inline-list navigation__menu',
          'echo'            => true,
          'fallback_cb'     => 'wp_page_menu',
          'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
          'depth'           => 0,
          );
        wp_nav_menu( $defaults );
        ?>
      </div>
    </div>
  </header>
