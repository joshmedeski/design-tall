<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
  <div class="row">
    <div class="small-12 medium-8 columns">
      <div class="panel">
        <h2 class="panel__title"><?php the_title(); ?></h2>
        <ul class="post__meta inline-list">
          <li><i class="icon ion-calendar"></i><?php the_date(); ?></li>
          <li><i class="icon ion-pricetag"></i> <?php $category = get_the_category(); echo $category[0]->cat_name; ?></li>
          <li><i class="icon ion-person"></i> <?php echo $author = get_the_author(); ?></li>
        </ul>
        <?php the_content(); ?>
      </div>
      <div class="panel">
        <?php comments_template(); ?>
      </div>
    </div>
    <div class="medium-4 columns">
      <?php get_sidebar(); ?>
    </div>
  </div>
<?php endwhile // end of the loop. ?>
</div>
<?php get_footer(); ?>
