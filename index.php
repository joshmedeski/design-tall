<?php get_header(); ?>

<div class="row">
  <div class="medium-8 columns">
    <?php while ( have_posts() ) : the_post(); ?>
      <div class="box">
        <h3><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>
        <ul class="post__meta inline-list">
          <li><i class="icon ion-calendar"></i><?php the_date(); ?></li>
          <li><i class="icon ion-pricetag"></i> <?php $category = get_the_category(); echo $category[0]->cat_name; ?></li>
          <li><i class="icon ion-person"></i> <?php echo $author = get_the_author(); ?></li>
        </ul>
        <div class="post__excerpt"><?php the_excerpt(); ?></div>
      </div>
    <?php endwhile // end of the loop. ?>
  </div>
  <div class="medium-4 columns">
    <?php get_sidebar(); ?>
  </div>
</div>


<?php get_footer(); ?>


